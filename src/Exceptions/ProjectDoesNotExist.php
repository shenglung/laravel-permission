<?php
/**
 * Project does not exist exception
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2019/01/15
 * @since 1.0.0 2019/01/15 12:35 PM init
 */

namespace Samslhsieh\Permission\Exceptions;

use InvalidArgumentException;
use Throwable;

class ProjectDoesNotExist extends InvalidArgumentException
{
    /**
     * ProjectDoesNotExist constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = "The project does not exist.";
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param int $id
     * @return ProjectDoesNotExist
     */
    public static function withId(int $id)
    {
        return new static("There is no [project] with id `{$id}`.");
    }

    /**
     * @param string $name
     * @return ProjectDoesNotExist
     */
    public static function withName(string $name)
    {
        return new static("There is no [project] with name `{$name}`.");
    }
}