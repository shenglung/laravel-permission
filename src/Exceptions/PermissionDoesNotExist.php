<?php
/**
 * Your file description
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2018/12/27
 * @since 1.0.0 2018/12/27 12:35 PM description
 */

namespace Samslhsieh\Permission\Exceptions;

use InvalidArgumentException;
use Throwable;

class PermissionDoesNotExist extends InvalidArgumentException
{
    /**
     * PermissionDoesNotExist constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = "The permission does not exist.";
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param int $id
     * @return PermissionDoesNotExist
     */
    public static function withId(int $id)
    {
        return new static("There is no [permission] with id `{$id}`.");
    }

    /**
     * @param string $name
     * @return PermissionDoesNotExist
     */
    public static function withName(string $name)
    {
        return new static("There is no [permission] with name `{$name}`.");
    }
}