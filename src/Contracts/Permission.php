<?php
/**
 * Permission Interface
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2018/12/26
 * @since 1.0.0 2018/12/26 4:14 PM init
 */

namespace Samslhsieh\Permission\Contracts;


use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface Permission
{
    /**
     * A permission can be applied to roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): BelongsToMany;

    /**
     * Find a permission by its name.
     *
     * @param string $name
     * @throws \Samslhsieh\Permission\Exceptions\PermissionDoesNotExist
     * @return Permission
     */
    public static function findByName(string $name): self;

    /**
     * Find a permission by its id.
     *
     * @param int $id
     * @throws \Samslhsieh\Permission\Exceptions\PermissionDoesNotExist
     * @return Permission
     */
    public static function findById(int $id): self;
}