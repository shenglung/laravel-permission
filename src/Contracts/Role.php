<?php
/**
 * Role Interface
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2018/12/26
 * @since 1.0.0 2018/12/26 4:14 PM init
 */

namespace Samslhsieh\Permission\Contracts;


use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface Role
{
    /**
     * A role may be given various permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions(): BelongsToMany;

    /**
     * A role belongs to single project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project(): BelongsTo;

    /**
     * Find a role by its name and guard name.
     *
     * @param string $name
     * @return Role
     * @throws \Samslhsieh\Permission\Exceptions\RoleDoesNotExist
     */
    public static function findByName(string $name): self;

    /**
     * Find a role by its id and guard name.
     *
     * @param int $id
     * @return Role
     * @throws \Samslhsieh\Permission\Exceptions\RoleDoesNotExist
     */
    public static function findById(int $id): self;
}