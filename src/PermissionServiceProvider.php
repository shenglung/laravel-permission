<?php

namespace Samslhsieh\Permission;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Samslhsieh\Permission\Models\Permission;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/permission.php' => config_path('permission.php'),
        ], "config");

        if (! class_exists('CreatePermissionSetupTables')) {
            $this->publishes([
                __DIR__.'/../database/migrations/2017_04_28_024424_create_permission_setup_table.php.stub' =>
                    database_path('migrations/'.date('Y_m_d_His', time()).'_create_permission_setup_table.php'),
            ], 'migrations');
        }

        $this->registerPermission();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/permission.php', 'permission');
    }

    public function registerPermission()
    {
        try{
            $permissions = Permission::active()
                ->with([
                    "roles" => function ($query) {
                        $query->whereActive(true);
                    }
                ])
                ->get();

            foreach ($permissions as $permission) {
                Gate::define($permission->name, function($user, $project = null) use ($permission) {
                    return $user->hasPermission($permission, $project);
                });
            }
        } catch (\Illuminate\Database\QueryException $e) {
            error_log( '[Query Exception] Failed to connect DB' );
        } catch(\PDOException $pdoe){
            error_log( '[PDOException] Failed to connect DB' );
        } catch (\Exception $e) {
            error_log("Failed to gate define, message : " . $e->getMessage());
        }
    }
}
