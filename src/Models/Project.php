<?php

namespace Samslhsieh\Permission\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Samslhsieh\Permission\Contracts\Project as ProjectContract;
use Samslhsieh\Permission\Exceptions\ProjectDoesNotExist;

class Project extends Model implements ProjectContract
{
    protected $fillable = [ "name", "label", "active", "memo" ];

    public function roles(): HasMany
    {
        return $this->hasMany(Role::class);
    }

    public static function findById(int $id): ProjectContract
    {
        $project = static::active()->find($id);

        if (! $project) {
            throw ProjectDoesNotExist::withId($id);
        }

        return $project;
    }

    public static function findByName(string $name): ProjectContract
    {
        $role = static::active()->where('name', $name)->first();

        if (! $role) {
            throw ProjectDoesNotExist::withName($name);
        }

        return $role;
    }
}
