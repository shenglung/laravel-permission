<?php

namespace Samslhsieh\Permission\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Samslhsieh\Permission\Contracts\Permission as PermissionContract;
use Samslhsieh\Permission\Exceptions\PermissionDoesNotExist;

class Permission extends Model implements PermissionContract
{
    protected $fillable = [ "name", "label", "active", "memo" ];

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @param int $id
     * @return Permission
     */
    public static function findById(int $id): PermissionContract
    {
        $permission = static::active()->find($id);

        if (! $permission) {
            throw PermissionDoesNotExist::withId($id);
        }

        return $permission;
    }

    /**
     * @param string $name
     * @return Permission
     */
    public static function findByName(string $name): PermissionContract
    {
        $permission = static::active()->where("name", $name)->first();

        if (! $permission) {
            throw PermissionDoesNotExist::withName($name);
        }

        return $permission;
    }
}
