<?php
/**
 * Base Eloquent Model
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2020/02/17
 * @since 1.0.0 2020/02/17 5:55 PM init
 */

namespace Samslhsieh\Permission\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{
    protected $dateFormat = 'U';

    protected $hidden = ["deleted_by", "deleted_at", "pivot"];

    protected $casts = [
        "active" => "boolean"
    ];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeRecent($query)
    {
        return $query->orderByDesc("id");
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }
}