<?php
/**
 * Your file description
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2018/12/27
 * @since 1.0.0 2018/12/27 5:13 PM description
 */

namespace Samslhsieh\Permission\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Samslhsieh\Permission\Contracts\Permission;
use Samslhsieh\Permission\Contracts\Role;
use Samslhsieh\Permission\Contracts\Project;
use Samslhsieh\Permission\Exceptions\PermissionDoesNotExist;
use Samslhsieh\Permission\Exceptions\ProjectDoesNotExist;

trait HasPermission
{
    private $permissionClass;
    private $projectClass;

    /**
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function getPermissionClass()
    {
        return $this->permissionClass = app(\Samslhsieh\Permission\Models\Permission::class);
    }

    public function getProjectClass()
    {
        return $this->projectClass = app(\Samslhsieh\Permission\Models\Project::class);
    }

    /**
     * @param int|string|Permission $permission
     * @param null $project
     * @return bool
     * @throws PermissionDoesNotExist
     */
    public function hasPermission($permission, $project = null): bool
    {
        $permission = $this->getStoredPermission($permission);

        if ($permission instanceof Permission) {
            if ($this instanceof Role) {
                return $permission->roles->filter(function ($role) {
                    return $role->active;
                })->contains($this);
            }

            if (isset($project)) {
                $project = $this->getStoredProject($project);

                if (!$project instanceof Project) {
                    throw new ProjectDoesNotExist();
                }

                $project = $project->id;
            }

            return $this->roles()
                    ->active()
                    ->where("project_id", $project)
                    ->get()->intersect($permission->roles)
                    ->isNotEmpty() ||
                $this->permissions()
                    ->active()
                    ->where("project_id", $project)
                    ->get()
                    ->contains($permission);
        }

        throw new PermissionDoesNotExist();
    }

    /**
     * An alias to hasPermission(), but avoids throwing an exception.
     *
     * @param int|string|Permission $permission
     * @param null $project
     * @return bool
     */
    public function checkPermission($permission, $project = null): bool
    {
        try {
            return $this->hasPermission($permission, $project);
        } catch (PermissionDoesNotExist | ProjectDoesNotExist $e) {
            return false;
        }
    }

    /**
     * Determine if the model has any of the given permissions.
     *
     * @param array ...$permissions
     * @return bool
     */
    public function hasAnyPermission($permissions, $projects = null): bool
    {
        if (! is_array($permissions)) {
            $permissions = [$permissions];
        }

        foreach ($permissions as $permission) {
            if ($this->checkPermission($permission, $projects)) {
               return true;
            }
        }

        return false;
    }

    /**
     * Determine if the model has all of the given permissions.
     *
     * @param array ...$permissions
     * @return bool
     */
    public function hasAllPermissions($permissions, $projects = null): bool
    {
        if (! is_array($permissions)) {
            $permissions = [$permissions];
        }

        foreach ($permissions as $permission) {
            if (! $this->checkPermission($permission, $projects)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Grant the given permission(s) to a role.
     *
     * @param int|string|array|Permission|\Illuminate\Support\Collection $permissions
     * @return $this
     * @throws PermissionDoesNotExist
     */
    public function givePermission(...$permissions)
    {
        if (is_array($permissions[0])) {
            $permissions = $permissions[0];
        }

        $permissions = collect($permissions)
            ->flatten()
            ->map(function ($permission) {
                return $this->getStoredPermission($permission);
            })
            ->filter(function ($permission) {
                return $permission instanceof Permission;
            })
            ->pluck("id");

        $this->permissions()->sync($permissions);
        $this->load("permissions");

        return $this;
    }

    /**
     * Revoke the given permission.
     *
     * @param int|string|Permission $permission
     * @return $this
     * @throws PermissionDoesNotExist
     */
    public function revokePermission($permission)
    {
        $this->permissions()->detach($this->getStoredPermission($permission));
        $this->load('permissions');

        return $this;
    }

    /**
     * Remove all current permissions and set the given ones.
     *
     * @param string|array|Permission|\Illuminate\Support\Collection $permissions
     * @return $this
     * @throws PermissionDoesNotExist
     */
    public function syncPermissions(...$permissions)
    {
        $this->permissions()->detach();
        return $this->givePermission($permissions);
    }

    /**
     * @param Builder $query
     * @param $permissions
     * @return Builder
     * @throws PermissionDoesNotExist
     */
    public function scopePermission(Builder $query, $permissions): Builder
    {
        if ($permissions instanceof Collection) {
            $permissions = $permissions->all();
        }

        if (! is_array($permissions)) {
            $permissions = [$permissions];
        }

        $permissions = array_map(function ($permission) {
            if ($permission instanceof Permission) {
                return $permission;
            }

            return $this->getStoredPermission($permission);
        }, $permissions);

        return $query->whereHas('permissions', function ($query) use ($permissions) {
                $query->where(function ($query) use ($permissions) {
                    foreach ($permissions as $permission) {
                        $query->orWhere(config('permission.table_names.permissions').'.id', $permission->id);
                    }
                });
            })
            ->orWhereHas("roles", function ($query) use ($permissions) {
                $query->where("active", true)->whereHas('permissions', function ($query) use ($permissions) {
                    $query->where(function ($query) use ($permissions) {
                        foreach ($permissions as $permission) {
                            $query->orWhere(config('permission.table_names.permissions').'.id', $permission->id);
                        }
                    });
                });
            });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\BelongsTo|Collection|object|null
     */
    public function getACL()
    {
        $acl = new class extends \stdClass {
            public function __toString() {
                return json_encode($this);
            }
        };

        if ($this instanceof Role) {
            $permissions = $this->permissions()->where("active", true)->get();
            $project = $this->project()->where("active", true)->first();

            if ($project) {
                $project->permissions = $permissions;
                $acl->obscurities = [];
                $acl->projects = [$project];

            } else {
                $acl->obscurities = $permissions;
                $acl->projects = [];
            }

            return $acl;
        }

        $obscurities = collect([]);
        $accessLists = collect([]);

        $roles = $this->roles()
            ->where("active", true)
            ->with(["permissions" => function ($query) {
                $query->where("active", true);
            }])
            ->get();

        $roles->each(function (Role $role) use (&$obscurities, $accessLists) {
            if (isset($role->project_id)) {
                $project = new \stdClass();
                $project->id = $role->project_id;
                $project->permissions = $role->permissions;
                $accessLists->push($project);
            } else {
                $obscurities = $obscurities->concat($role->permissions);
            }
        });

        $permissions = $this->permissions()->where("active", true)->get();

        $permissions->each(function (Permission $permission) use (&$obscurities, $accessLists) {
            if(isset($permission->pivot->project_id)) {
                $project = new \stdClass();
                $project->id = $permission->pivot->project_id;
                $project->permissions = [$permission];
                $accessLists->push($project);
            } else {
                !$obscurities->contains(function ($value, $key) use ($permission) {
                    return $permission->id === $value->id;
                })? $obscurities->push($permission) : null;
            }
        });

        $projects = $this->getProjectClass()
            ->where("active", true)
            ->find($accessLists->unique("id")->pluck("id"));

        $projects->transform(function (Project $project) use ($accessLists) {
            $project->permissions = $accessLists->filter(function ($value, $key) use ($project) {
                    if ($project->id===$value->id) {
                        return true;
                    }
                })
                ->map(function ($item, $key) {
                    return $item->permissions;
                })
                ->flatten()
                ->unique("id")
                ->sortBy("id")
                ->values();
            return $project;
        });

        $acl->obscurities = $obscurities->unique("id")
            ->sortBy("id")
            ->values();

        $acl->projects = $projects;

        return $acl;
    }

    /**
     * @param $permissions
     * @return Permission|Collection
     * @throws PermissionDoesNotExist
     */
    protected function getStoredPermission($permissions)
    {
        $permissionClass = $this->getPermissionClass();

        if (is_numeric($permissions)) {
            return $permissionClass->findById($permissions);
        }

        if (is_string($permissions)) {
            return $permissionClass->findByName($permissions);
        }

        if (is_array($permissions)) {
            return $permissionClass
                ->where("active", true)
                ->whereIn('name', $permissions)
                ->get();
        }

        if ($permissions instanceof Permission) {
            return $permissions;
        }

        throw new PermissionDoesNotExist();
    }

    /**
     * @param $projects
     * @return mixed
     */
    protected function getStoredProject($projects)
    {
        $projectClass = $this->getProjectClass();

        if (is_numeric($projects)) {
            return $projectClass->findById($projects);
        }

        if (is_string($projects)) {
            return $projectClass->findByName($projects);
        }

        if (is_array($projects)) {
            return $projectClass
                ->where("active", true)
                ->whereIn('name', $projects)
                ->get();
        }

        if ($projects instanceof Project) {
            return $projects;
        }

        throw new ProjectDoesNotExist();
    }
}
