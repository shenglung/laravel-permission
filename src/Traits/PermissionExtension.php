<?php
/**
 * Your file description
 *
 * @version 1.0.0
 * @author Sam S.L Hsieh shenglung0619@gmail.com
 * @date 2018/12/27
 * @since 1.0.0 2018/12/27 3:54 PM description
 */

namespace Samslhsieh\Permission\Traits;

use Samslhsieh\Permission\Models\Role;
use Samslhsieh\Permission\Models\Permission;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait PermissionExtension
{
    use HasRole, HasPermission;

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, "role_user");
    }

    /**
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class, "permission_user")->withPivot('project_id');
    }
}
