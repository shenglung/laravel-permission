# Laravel Permission

Laravel Permission is a laravel package for can be generated fast permission framework


## Getting Started


### Prerequisites

此插件包允許使用自己的 User 資料表，也可以使用此插件包提供的。
在使用插件包前，須先將 User Eloquent Model 設定好，如果沒有，請產生一個

```
$ php artisan make:model User
```

### Installing

請先在 composer.json的repositories 加入插件包來源

```
"require-dev": {...},
"repositories": [
       {
            "type": "vcs",
            "url":  "https://bitbucket.org/shenglung/laravel-permission.git"
        }
    ],
"autoload": {...}
```

查找插件包，確認是否配置正常

```
$ composer search samslhsieh
```

正確會返回插件包資訊

```
samslhsieh/laravel-permission laravel-permission is a laravel package for can be generated fast permission framework
```

安裝插件包

```
$ composer require samslhsieh/laravel-permission
```

Laravel `>5.5` 使用 Package Auto-Discovery，該 Service Provider 會自動註冊  
若 Laravel `<=5.4` 須在 config\app.php 中添加此行

```
'providers' => [
       //...
       Samslhsieh\Permission\PermissionServiceProvider::class,
],
```

安裝完成後發佈並執行 Migration 建立資料表格

```
$ php artisan vendor:publish --provider="Samslhsieh\Permission\PermissionServiceProvider" --tag="migrations"
$ php artisan migrate
```

發佈設定檔

```
$ php artisan vendor:publish --provider="Samslhsieh\Permission\PermissionServiceProvider" --tag="config"
```

在 User Eloquent Model 表格添加此 Trait

```
class User {
    ...
    use \Samslhsieh\Permission\Traits\PermissionExtension;
    ...
}
```

安裝完成

## Usage


建立資料

```
use Samslhsieh\Permission\Models\Role;
use Samslhsieh\Permission\Models\Permission;

$role = Role::create(['name' => 'writer']);
$permission = Permission::create(['name' => 'edit-articles']);
```

### 使用者直接操作、檢查權限

先取得使用者 Eloquent

```
// 查詢使用者
$user = User::find(1);

// 或是透過登入取得使用者
$user = \Auth::user();
```

查詢使用者可使用的權限

```
$user->getACL();
```

向使用者授予權限

```
// 給予權限
$user->givePermission('edit');

// 您也可以使用權限 ID(int) 或 Name(sting) 當作參數，或使用多重參數
$user->givePermission('edit-articles', 3);

// 或使用陣列當作參數
$user->givePermission(['edit-articles', 3]);
```

撤銷使用者的權限

```
$user->revokePermission('edit-articles');

// 或使用陣列當作參數
$user->revokePermission(['edit-articles', 3]);
```

更新使用者的權限，除了給定的權限外，該使用者原本擁有的其他權限將會被刪除

```
$user->syncPermissions('edit-articles');
$user->syncPermissions('edit-articles', 3);
$user->syncPermissions(['edit-articles', 3]);
```

檢查使用者是否擁有某項權限，可接受 Permission `id`、`name` 或是 `Instance`

```
$user->hasPermission(1)
$user->hasPermission("permission1");
$user->hasPermission($permission);
```

也可以加入比對專案功能，可接受 Project `id`、`name` 或是 `Instance`

```
$user->hasPermission(1, 1)
$user->hasPermission("permission1", "project1");
$user->hasPermission($permission, $project);
```

`hasPermission()` 若找不到權限資料會拋出 `PermissionDoesNotExist` Exception，
若不想處理錯誤可使用 `checkPermission()` 代替

```
$user->checkPermission(1);
$user->checkPermission("permission1");
$user->checkPermission($permission);

$user->checkPermission("permission1", "project1");
```

您可以檢查使用者是否具有任何一組權限：

```
$user->hasAnyPermission(['edit-articles', 'publish-articles', 'unpublish-articles']);
```

或者使用者必須擁有所有的權限

```
$user->hasAllPermissions(['edit-articles', 'publish-articles', 'unpublish-articles']);
```

您也可以使用權限ID(int)當作參數

```
$user->hasAnyPermission(['edit-articles', 1, 5]);
```

同樣也可以加入 `project` 作為篩選的條件，但專案不能接受陣列參數

```
$user->hasAnyPermission(['edit-articles', 1, 5], "project_1");
```

### 透過角色使用權限

查詢該角色所擁有的權限

```
$role->permissions
```

向角色授予權限

```
$role = Role::find(1);

$role->givePermission('edit');

// 您也可以使用權限ID(int)或Name(sting)當作參數，或使用多重參數
$role->givePermission('edit-articles', 3);

// 或使用陣列當作參數
$role->givePermission(['edit-articles', 3]);
```

撤銷角色的權限

```
$role->revokePermission('edit-articles');

// 或使用陣列當作參數
$role->revokePermission(['edit-articles', 3]);
```

更新角色的權限，除了給定的權限外，該角色原本擁有的其他權限將會被刪除

```
$role->syncPermissions('edit-articles');
$role->syncPermissions('edit-articles', 3);
$role->syncPermissions(['edit-articles', 3]);
```

檢查角色是否擁有某項權限，可接受Permission `id`、`name` 或是 `Instance`

```
$role->hasPermission(1)
$role->hasPermission("permission1");
$role->hasPermission($permission);
```

`hasPermission()` 若找不到權限資料會拋出 `PermissionDoesNotExist` Exception，
若不想處理錯誤可使用 `checkPermission()` 代替

```
$role->checkPermission(1);
$role->checkPermission("permission1");
$role->checkPermission($permission);
```

您可以檢查角色是否具有任何一組權限：

```
$role->hasAnyPermission(['edit-articles', 'publish-articles', 'unpublish-articles']);
```

或者角色必須擁有所有的權限

```
$role->hasAllPermissions(['edit-articles', 'publish-articles', 'unpublish-articles']);
```

您也可以使用權限 ID(int) 當作參數

```
$role->hasAnyPermission(['edit-articles', 1, 5]);
```




### 透過使用者操作、檢查角色

賦予使用者角色

```
$user->assignRole('writer');

// 您也可以使用權限 ID(int) 或 Name(sting) 當作參數，或使用多重參數
$user->assignRole('writer', 'admin');

// 或使用陣列當作參數
$user->assignRole(['writer', 'admin']);
```

撤銷使用者角色

```
$user->removeRole('admin');

// 或使用陣列當作參數
$user->removeRole(['admin', 3]);
```

更新使用者的角色，除了給定的角色外，該使用者原本擁有的其他角色將會被刪除

```
$user->syncRoles('edit-articles');
$user->syncRoles('edit-articles', 3);
$user->syncRoles(['edit-articles', 3]);
```

檢查使用者是否擁有某個角色，可接受 Role `id`、`name` 或是 `Instance`

```
$user->hasRole(1)
$user->hasRole("role1");
$user->hasRole($role);
```

`hasRole()`若找不到權限資料會拋出 `RoleDoesNotExist` Exception，
若不想處理錯誤可使用 `checkRole()` 代替

```
$user->checkRole(1);
$user->checkRole("role1");
$user->checkRole($role);
```

您可以檢查使用者是否具有任何一組角色：

```
$user->hasAnyRole(['admin', 'writer', 'role3']);
```

或者使用者必須擁有所有的角色

```
$user->hasAllRoles(['admin', 'writer', 'role3']);
```

您也可以使用角色 ID(int) 當作參數

```
$user->hasAnyRole(['admin', 1, 5]);
```

### Syntactic Sugar

**此章節的語法不支援專案 (projects) 功能**

該使用者是否擁有此權限

```
$user->can("permission");
```

當前使用者是否可以存取此路由

```
Route::get("articles", "ArticleController@index")
    ->middleware("can:view-articles");
```

在 Controller 中，利用輔助函式檢查權限，取代 `if` `else` 

```
$this->authorize("view-articles");
```

Blade

```
@can("admin")
    <!-- 管理者權限可以存取 -->
@elsecan("view-articles")
    <!-- 可視文章列表的使用者可以存取 -->
@endcan

@cannot("admin")
    <!-- 管理者權限不可以存取 -->
@elsecannot("view-articles")
    <!-- 可視文章列表的使用者不可以存取 -->
@endcannot

```

## Authors

* **Sam S.L Hsieh** - *Initial work* - [Laravel-Permission](https://bitbucket.org/shenglung/laravel-permission)
